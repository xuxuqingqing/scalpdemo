package com.demo.scalp.data.source;

import com.demo.scalp.entity.DetailEntity;
import com.demo.scalp.entity.UpLoadImageEntity;

import io.reactivex.Observable;
import me.goldze.mvvmhabit.http.BaseResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by goldze on 2019/3/26.
 */
public interface HttpDataSource {
    //获取检测结果
    Observable<DetailEntity> getDetail(String image1, String image2, String image3);

    //上传图片
    Observable<UpLoadImageEntity> uploadInmage(MultipartBody.Part file, RequestBody app_name);
}
