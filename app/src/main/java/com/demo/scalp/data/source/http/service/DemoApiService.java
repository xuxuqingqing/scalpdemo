package com.demo.scalp.data.source.http.service;

import com.demo.scalp.entity.DetailEntity;
import com.demo.scalp.entity.UpLoadImageEntity;

import io.reactivex.Observable;
import me.goldze.mvvmhabit.http.BaseResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by goldze on 2017/6/15.
 * 65535
 */

public interface DemoApiService {
    @FormUrlEncoded
    @POST("/v2/api/skin/analysis/65024")
    Observable<DetailEntity> getDetail(@Field("image") String image, @Field("image_pl") String image2, @Field("image_uv") String image3);

    @Multipart
    @POST("/fileSvr/put")
    Observable<UpLoadImageEntity> uploadInmage(@Part MultipartBody.Part file, @Part("app_name") RequestBody app_name);
}
