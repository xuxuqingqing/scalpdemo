package com.demo.scalp.data.source.http;

import com.demo.scalp.data.source.HttpDataSource;
import com.demo.scalp.data.source.http.service.DemoApiService;
import com.demo.scalp.entity.DetailEntity;
import com.demo.scalp.entity.UpLoadImageEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import me.goldze.mvvmhabit.http.BaseResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by goldze on 2019/3/26.
 */
public class HttpDataSourceImpl implements HttpDataSource {
    private DemoApiService apiService;
    private volatile static HttpDataSourceImpl INSTANCE = null;

    public static HttpDataSourceImpl getInstance(DemoApiService apiService) {
        if (INSTANCE == null) {
            synchronized (HttpDataSourceImpl.class) {
                if (INSTANCE == null) {
                    INSTANCE = new HttpDataSourceImpl(apiService);
                }
            }
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    private HttpDataSourceImpl(DemoApiService apiService) {
        this.apiService = apiService;
    }

    @Override
    public Observable<DetailEntity> getDetail(String image1, String image2, String image3) {
        return apiService.getDetail(image1,image2,image3);
    }

    @Override
    public Observable<UpLoadImageEntity> uploadInmage(MultipartBody.Part file, RequestBody app_name) {
        return apiService.uploadInmage(file,app_name);
    }
}
