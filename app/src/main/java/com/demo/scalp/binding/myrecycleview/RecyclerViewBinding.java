package com.demo.scalp.binding.myrecycleview;

import android.databinding.BindingAdapter;
import android.databinding.ObservableList;
import android.os.Build;
import android.support.annotation.LayoutRes;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;

import com.demo.scalp.view.MyRecyclerView;

import java.util.Objects;

public class RecyclerViewBinding {
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @BindingAdapter(value = {"layoutId", "list","itemClickListener"
    }, requireAll = false)
    public static <T> void setAdapter(MyRecyclerView view,
                                      @LayoutRes final int layoutId,
                                      ObservableList<T> list,
                                      OnRecyclerItemClickListener itemClickListener){
        if (list == null) {
            return;
        }

        if (view.getLayoutManager() == null) {
            // default LinearLayoutManager 垂直布局
            LinearLayoutManager layoutManager = new LinearLayoutManager(
                    view.getContext(), LinearLayoutManager.VERTICAL, false);
            view.setLayoutManager(layoutManager);

            DividerItemDecoration decoration = new DividerItemDecoration(
                    view.getContext(), LinearLayoutManager.VERTICAL);
            //noinspection ConstantConditions 添加分割线
            //decoration.setDrawable(ContextCompat.getDrawable(view.getContext(), R.drawable.divider_gray));

            view.addItemDecoration(decoration);
        }
        //noinspection unchecked 绑定adpter
        BindAdapter<T> bindAdapter = (BindAdapter<T>) view.getAdapter();
        if (bindAdapter == null || !Objects.equals(bindAdapter.getObservableList(), list)) {

            //创建适配器
            bindAdapter = new BindAdapter<T>(view, list) {
                @Override
                public int onCreateViewHolderLayoutId() {
                    return layoutId;
                }
            };
        }

        //设置适配器
        view.setAdapter(bindAdapter);

        //item点击事件
        if (itemClickListener != null) {
            view.setOnRecyclerItemClickListener(itemClickListener);
        }
    }
}
