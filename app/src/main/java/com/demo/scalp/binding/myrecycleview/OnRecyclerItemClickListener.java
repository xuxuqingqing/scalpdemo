package com.demo.scalp.binding.myrecycleview;

public interface OnRecyclerItemClickListener {
    void onRecyclerItemClick(Object item);
}
