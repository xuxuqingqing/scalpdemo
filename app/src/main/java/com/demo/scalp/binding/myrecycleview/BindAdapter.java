package com.demo.scalp.binding.myrecycleview;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableList;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.demo.scalp.BR;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.annotations.NonNull;

public abstract  class BindAdapter<T> extends RecyclerView.Adapter<BindAdapter.BindHolder> {
    private List<T> list = new ArrayList<>();
    private ObservableList<T> observableList;

    private OnRecyclerItemClickListener itemOneClickListener;

    public BindAdapter(final RecyclerView view, ObservableList<T> observableList) {

        this.observableList = observableList;
        this.list.addAll(observableList);

        //添加数据监听
        observableList.addOnListChangedCallback(new ObservableList.OnListChangedCallback<ObservableList<T>>() {
            @Override
            public void onChanged(ObservableList<T> sender) {

            }

            @Override
            public void onItemRangeChanged(ObservableList<T> sender, int positionStart,
                                           int itemCount) {
                notifyItemRangeChanged(positionStart,itemCount);
            }

            @Override
            public void onItemRangeInserted(final ObservableList<T> sender, final int positionStart,
                                            final int itemCount) {
                view.post(new Runnable() {
                    @Override
                    public void run() {
                        list.add(positionStart, sender.get(positionStart));
                        notifyItemRangeInserted(positionStart, itemCount);
                        view.scrollToPosition(positionStart);
                    }
                });

            }

            @Override
            public void onItemRangeMoved(ObservableList<T> sender, int fromPosition,
                                         int toPosition, int itemCount) {

            }

            @Override
            public void onItemRangeRemoved(ObservableList<T> sender, final int positionStart, final int itemCount) {
                view.post(new Runnable() {
                    @Override
                    public void run() {
                        if (itemCount == 1) {
                            list.remove(positionStart);
                            notifyItemRangeRemoved(positionStart, itemCount);

                            notifyItemRangeChanged(positionStart, getItemCount() - positionStart, new Object());

                        } else {
                            list.clear();
                            notifyDataSetChanged();
                        }
                    }
                });
            }
        });
    }

    public OnRecyclerItemClickListener getItemOneClickListener() {
        return itemOneClickListener;
    }

    public ObservableList<T> getObservableList() {
        return observableList;
    }

    public void setObservableList(ObservableList<T> observableList) {
        this.observableList = observableList;
    }

    public void setItemOneClickListener(OnRecyclerItemClickListener itemOneClickListener) {
        this.itemOneClickListener = itemOneClickListener;
    }

    public abstract int onCreateViewHolderLayoutId();

    @NonNull
    @Override
    public BindHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewDataBinding binding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                        onCreateViewHolderLayoutId(), parent, false);


        return new BindAdapter.BindHolder(binding);
    }

    @Override
    public void onBindViewHolder( @NonNull final BindHolder holder,int position) {
        holder.bind(this.list.get(holder.getAdapterPosition()),
                new OnRecyclerItemClickListener() {
                    @Override
                    public void onRecyclerItemClick(Object item) {
                        if (itemOneClickListener != null) {

                            itemOneClickListener.onRecyclerItemClick(item);
                        }
                    }
                });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemOneClickListener != null) {
                    itemOneClickListener.onRecyclerItemClick(getItem(holder.getAdapterPosition()));
                }
            }
        });
    }

    public T getItem(int position) {
        if (position < this.list.size()) {
            return this.list.get(position);
        } else {
            return null;
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    /**
     * 自定义ViewHolder
     */
    public static class BindHolder extends RecyclerView.ViewHolder {

        ViewDataBinding mBinding;

        public BindHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        public void bind(Object obj,
                         OnRecyclerItemClickListener itemOneClickListener) {
            //this.mBinding.setVariable(BR.item, obj);
            //this.mBinding.setVariable(BR.itemOneClickListener, itemOneClickListener);
            this.mBinding.executePendingBindings();

        }
    }
}
