package com.demo.scalp.entity;

import com.google.gson.annotations.SerializedName;

public class DetailEntity {
    public int code;
    public int error_detect_types;
    public String filename;
    public String detect_types;

    public ScalpStatus scalp_status;
    public HairPore hair_pore;
    public ScalpKeratin scalp_keratin;
    public ScalpVessel scalp_vessel;
    public HairLost hair_lost;
    public HairDensity hair_density;
    public HairThickness hair_thickness;
//    public Corneum corneum;
//    public Sensitivity sensitivity;
//    public Elasticity elasticity;
//    public Color color;
//    public Blackhead blackhead;
//    public Pore pore;
//    public Roughness roughness;
//    public Pigment pigment;
//    public Skintype skin_type;

    public String id;
    public String filename_pl;
    public String filename_uv;

//    //ok
//    public class Corneum{
//        public double score;
//        public String level;
//        public String filename;
//        public double area;
//    }
//
//    //ok
//    public class Sensitivity{
//        public double score;
//        public String level;
//        public String filename;
//        public double area;
//    }
//
//    //ok
//    public class Elasticity{
//        public double score;
//        public String level;
//        public String filename;
//        public double area;
//    }
//
//    //ok
//    public class Color{
//        public String result;
//    }
//
//    //ok
//    public class Blackhead{
//        public int count;
//        public double score;
//        public String level;
//        public String filename;
//        public double area;
//    }
//
//    //ok
//    public class Pore{
//        public double score;
//        public String level;
//        public String filename;
//        public int count;
//        public double area;
//    }
//
//    public class Roughness{
//        public double score;
//        public String level;
//        public double result;
//    }
//
//    public class Pigment{
//        public double score;
//        public String filename;
//        public double area;
//    }
//
//    //OK
//    public class Skintype{
//        public double score;
//        public String level;
//        public String filename;
//        public double area;
//    }

    //ok
    public class ScalpStatus{
        public double score;
        public String level;
        public String filename;
    }

    //ok
    public class HairPore{
        public double score;
        public String level;
        public String filename;
    }

    //ok
    public class ScalpKeratin{
        public double score;
        public String level;
        public String filename;
    }

    //ok
    public class ScalpVessel{
        public double score;
        public String level;
        public String filename;
    }

    //ok
    public class HairLost{
        public double score;
        public String level;
        public String filename;
    }

    //ok
    public class HairDensity{
        public double score;
        public String level;
        public String filename;
        public double density;
    }

    //OK
    public class HairThickness{
        public double score;
        public String level;
        public double thickness;
        public String filename;
    }
}
