package com.demo.scalp.entity;

import android.content.res.Resources;

import com.demo.scalp.R;

import java.util.ArrayList;
import java.util.Arrays;

public class ShowEntity {

    public ShowEntity(DetailEntity detailEntity, Resources resources, String BaseUrl)
    {
//        score = String.valueOf((int)Math.round((detailEntity.scalp_keratin.score +
//                detailEntity.scalp_status.score +
//                detailEntity.scalp_vessel.score +
//                detailEntity.hair_density.score +
//                detailEntity.hair_lost.score +
//                detailEntity.hair_pore.score +
//                detailEntity.hair_thickness.score)/7));

        score = String.valueOf((int)Math.round(detailEntity.scalp_keratin.score * 0.15 +
                detailEntity.scalp_status.score * 0.15 +
                detailEntity.scalp_vessel.score * 0.12 +
                detailEntity.hair_density.score * 0.12 +
                detailEntity.hair_lost.score * 0.16 +
                detailEntity.hair_pore.score * 0.2 +
                detailEntity.hair_thickness.score * 0.1));

        scalp_status = new ShowEntity.ScalpStatus();
        scalp_status.score = String.valueOf((int)Math.round(detailEntity.scalp_status.score));
        ArrayList<String> ScalpStatusList = scalp_status.getLevelAndDesc(detailEntity.scalp_status.level,resources);
        scalp_status.level = ScalpStatusList.get(0);
        scalp_status.desc = ScalpStatusList.get(1);
        scalp_status.barscore = ScalpStatusList.get(2);
        scalp_status.filename = String.format("%s%s",BaseUrl,detailEntity.scalp_status.filename);
        scalp_status.info = resources.getString(R.string.scalp_status_desc);
        scalp_status.list = scalp_status.getLevelArr(resources);

        hair_pore = new ShowEntity.HairPore();
        hair_pore.score = String.valueOf((int)Math.round(detailEntity.hair_pore.score));
        ArrayList<String> HairPoreList = hair_pore.getLevelAndDesc(detailEntity.hair_pore.level,resources);
        hair_pore.level = HairPoreList.get(0);
        hair_pore.desc = HairPoreList.get(1);
        hair_pore.barscore = HairPoreList.get(2);
        hair_pore.filename = String.format("%s%s",BaseUrl,detailEntity.hair_pore.filename);
        hair_pore.info = resources.getString(R.string.hair_pore_desc);
        hair_pore.list = hair_pore.getLevelArr(resources);

        scalp_keratin = new ShowEntity.ScalpKeratin();
        scalp_keratin.score = String.valueOf((int)Math.round(detailEntity.scalp_keratin.score));
        ArrayList<String> ScalpKeratinList = scalp_keratin.getLevelAndDesc(detailEntity.scalp_keratin.level,resources);
        scalp_keratin.level = ScalpKeratinList.get(0);
        scalp_keratin.desc = ScalpKeratinList.get(1);
        scalp_keratin.barscore = ScalpKeratinList.get(2);
        scalp_keratin.filename = String.format("%s%s",BaseUrl,detailEntity.scalp_keratin.filename);
        scalp_keratin.info = resources.getString(R.string.scalp_keratin_desc);
        scalp_keratin.list = scalp_keratin.getLevelArr(resources);

        scalp_vessel = new ShowEntity.ScalpVessel();
        scalp_vessel.score = String.valueOf((int)Math.round(detailEntity.scalp_vessel.score));
        ArrayList<String> ScalpVesselList = scalp_vessel.getLevelAndDesc(detailEntity.scalp_vessel.level,resources);
        scalp_vessel.level = ScalpVesselList.get(0);
        scalp_vessel.desc = ScalpVesselList.get(1);
        scalp_vessel.barscore = ScalpVesselList.get(2);
        scalp_vessel.filename = String.format("%s%s",BaseUrl,detailEntity.scalp_vessel.filename);
        scalp_vessel.info = resources.getString(R.string.scalp_vessel_desc);
        scalp_vessel.list = scalp_vessel.getLevelArr(resources);

        hair_lost = new ShowEntity.HairLost();
        hair_lost.score = String.valueOf((int)Math.round(detailEntity.hair_lost.score));
        ArrayList<String> HairLostList = hair_lost.getLevelAndDesc(detailEntity.hair_lost.level,resources);
        hair_lost.level = HairLostList.get(0);
        hair_lost.desc = HairLostList.get(1);
        hair_lost.barscore = HairLostList.get(2);
        hair_lost.filename = String.format("%s%s",BaseUrl,detailEntity.hair_lost.filename);
        hair_lost.info = resources.getString(R.string.hair_lost_desc);
        hair_lost.list = hair_lost.getLevelArr(resources);

        hair_density = new ShowEntity.HairDensity();
        hair_density.score = String.valueOf((int)Math.round(detailEntity.hair_density.score));
        ArrayList<String> HairDensityList = hair_density.getLevelAndDesc(detailEntity.hair_density.level,resources);
        hair_density.level = HairDensityList.get(0);
        hair_density.desc = HairDensityList.get(1);
        hair_density.barscore = HairDensityList.get(2);
        hair_density.filename = String.format("%s%s",BaseUrl,detailEntity.hair_density.filename);
        hair_density.info = resources.getString(R.string.hair_density_desc);
        hair_density.list = hair_density.getLevelArr(resources);

        hair_thickness = new ShowEntity.HairThickness();
        hair_thickness.score = String.valueOf((int)Math.round(detailEntity.hair_thickness.score));
        ArrayList<String> HairThicknessList = hair_thickness.getLevelAndDesc(detailEntity.hair_thickness.level,resources);
        hair_thickness.level = HairThicknessList.get(0);
        hair_thickness.desc = HairThicknessList.get(1);
        hair_thickness.barscore = HairThicknessList.get(2);
        hair_thickness.filename = String.format("%s%s",BaseUrl,detailEntity.hair_thickness.filename);
        hair_thickness.info = resources.getString(R.string.hair_thickness_desc);
        hair_thickness.list = hair_thickness.getLevelArr(resources);

//        corneum = new ShowEntity.Corneum();
//        corneum.score = String.valueOf((int)Math.round(detailEntity.corneum.score));
//        ArrayList<String> corneumList = corneum.getLevelAndDesc(detailEntity.corneum.level,resources);
//        corneum.level = corneumList.get(0);
//        corneum.desc = corneumList.get(1);
//        corneum.info = resources.getString(R.string.corneum_desc);
//        corneum.list = corneum.getLevelArr(resources);
//
//        sensitivity = new ShowEntity.Sensitivity();
//        sensitivity.score = String.valueOf((int)Math.round(detailEntity.sensitivity.score));
//        ArrayList<String> sensitivityList = sensitivity.getLevelAndDesc(detailEntity.sensitivity.level,resources);
//        sensitivity.level = sensitivityList.get(0);
//        sensitivity.desc = sensitivityList.get(1);
//        sensitivity.filename = String.format("%s%s",BaseUrl,detailEntity.sensitivity.filename);
//        sensitivity.info = resources.getString(R.string.sensitivity_desc);
//        sensitivity.list = sensitivity.getLevelArr(resources);
//
//        elasticity = new ShowEntity.Elasticity();
//        elasticity.score = String.valueOf((int)Math.round(detailEntity.elasticity.score));
//        ArrayList<String> elasticityList = elasticity.getLevelAndDesc(detailEntity.elasticity.level,resources);
//        elasticity.level = elasticityList.get(0);
//        elasticity.desc = elasticityList.get(1);
//        elasticity.filename = String.format("%s%s",BaseUrl,detailEntity.elasticity.filename);
//        elasticity.info = resources.getString(R.string.elasticity_desc);
//        elasticity.list = elasticity.getLevelArr(resources);
//
//        color = new ShowEntity.Color();
//        ArrayList<String> colorList = color.getLevelAndDesc(detailEntity.color.result,resources);
//        color.level = colorList.get(0);
//        color.desc = colorList.get(1);
//        color.info = resources.getString(R.string.color_desc);
//        color.list = color.getLevelArr(resources);
//
//        blackhead = new ShowEntity.Blackhead();
//        blackhead.score = String.valueOf((int)Math.round(detailEntity.blackhead.score));
//        ArrayList<String> blackheadList = blackhead.getLevelAndDesc(detailEntity.blackhead.level,resources);
//        blackhead.level = blackheadList.get(0);
//        blackhead.desc = blackheadList.get(1);
//        blackhead.filename = String.format("%s%s",BaseUrl,detailEntity.blackhead.filename);
//        blackhead.info = resources.getString(R.string.blackhead_desc);
//        blackhead.list = blackhead.getLevelArr(resources);
//
//        pore = new ShowEntity.Pore();
//        pore.score = String.valueOf((int)Math.round(detailEntity.pore.score));
//        ArrayList<String> poreList = pore.getLevelAndDesc(detailEntity.pore.level,resources);
//        pore.level = poreList.get(0);
//        pore.desc = poreList.get(1);
//        pore.filename = String.format("%s%s",BaseUrl,detailEntity.pore.filename);
//        pore.info = resources.getString(R.string.pore_desc);
//        pore.list = pore.getLevelArr(resources);
//
//        roughness = new ShowEntity.Roughness();
//        roughness.score = String.valueOf((int)Math.round(detailEntity.roughness.score));
//        ArrayList<String> roughnessList = roughness.getLevelAndDesc(detailEntity.roughness.level,resources);
//        roughness.level = roughnessList.get(0);
//        roughness.desc = roughnessList.get(1);
//        roughness.info = resources.getString(R.string.roughness_desc);
//        roughness.list = roughness.getLevelArr(resources);
//
//        pigment = new ShowEntity.Pigment();
//        pigment.score = String.valueOf((int)Math.round(detailEntity.pigment.score));
//        ArrayList<String> pigmentList = pigment.getLevelAndDesc("",resources);
//        pigment.level = pigmentList.get(0);
//        pigment.desc = pigmentList.get(1);
//        pigment.filename = String.format("%s%s",BaseUrl,detailEntity.pigment.filename);
//        pigment.info = resources.getString(R.string.pigment_desc);
//        pigment.list = pigment.getLevelArr(resources);
//
//        skin_type = new ShowEntity.Skintype();
//        skin_type.score = String.valueOf((int)Math.round(detailEntity.skin_type.score));
//        ArrayList<String> skin_typeList = skin_type.getLevelAndDesc(detailEntity.skin_type.level,resources);
//        skin_type.level = skin_typeList.get(0);
//        skin_type.desc = skin_typeList.get(1);
//        skin_type.filename = String.format("%s%s",BaseUrl,detailEntity.skin_type.filename);
//        skin_type.info = resources.getString(R.string.skin_type_desc);
//        skin_type.list = skin_type.getLevelArr(resources);

    }

    public String score;
    public ShowEntity.ScalpStatus scalp_status;
    public ShowEntity.HairPore hair_pore;
    public ShowEntity.ScalpKeratin scalp_keratin;
    public ShowEntity.ScalpVessel scalp_vessel;
    public ShowEntity.HairLost hair_lost;
    public ShowEntity.HairDensity hair_density;
    public ShowEntity.HairThickness hair_thickness;

    public ShowEntity.Corneum corneum;
    public ShowEntity.Sensitivity sensitivity;
    public ShowEntity.Elasticity elasticity;
    public ShowEntity.Color color;
    public ShowEntity.Blackhead blackhead;
    public ShowEntity.Pore pore;
    public ShowEntity.Roughness roughness;
    public ShowEntity.Pigment pigment;
    public ShowEntity.Skintype skin_type;

    public class Corneum{
        public String score;
        public String level;
        //public String filename;
        public String desc;
        public String info;
        public ArrayList<String> list;
        public String barscore;

        public ArrayList<String> getLevelAndDesc(String level, Resources resources){
            switch (level)
            {
                case "severe":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.corneum_severe), resources.getString(R.string.corneum_severe_desc)));
                case "moderately":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.corneum_moderately), resources.getString(R.string.corneum_moderately_desc)));
                case "lightly":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.corneum_lightly), resources.getString(R.string.corneum_lightly_desc)));
                case "none":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.corneum_none), resources.getString(R.string.corneum_none_desc)));
                default:
                    return new ArrayList<String>(Arrays.asList("ない","ない"));
            }
        }

        public ArrayList<String> getLevelArr(Resources resources){
            return new ArrayList<String>(Arrays.asList(resources.getString(R.string.corneum_severe),
                    resources.getString(R.string.corneum_moderately),
                    resources.getString(R.string.corneum_lightly),
                    resources.getString(R.string.corneum_none)));
        }
    }

    public class Sensitivity{
        public String score;
        public String level;
        public String filename;
        public String desc;
        public String info;
        public ArrayList<String> list;
        public String barscore;

        public ArrayList<String> getLevelAndDesc(String level, Resources resources){
            switch (level)
            {
                case "severe":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.sensitivity_severe), resources.getString(R.string.sensitivity_severe_desc)));
                case "moderately":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.sensitivity_moderately), resources.getString(R.string.sensitivity_moderately_desc)));
                case "lightly":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.sensitivity_lightly), resources.getString(R.string.sensitivity_lightly_desc)));
                case "none":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.sensitivity_none), resources.getString(R.string.sensitivity_none_desc)));
                default:
                    return new ArrayList<String>(Arrays.asList("ない","ない"));
            }
        }

        public ArrayList<String> getLevelArr(Resources resources){
            return new ArrayList<String>(Arrays.asList(resources.getString(R.string.sensitivity_severe),resources.
                    getString(R.string.sensitivity_moderately),resources.
                    getString(R.string.sensitivity_lightly),resources.
                    getString(R.string.sensitivity_none)));
        }
    }

    public class Elasticity{
        public String score;
        public String level;
        public String filename;
        public String desc;
        public String info;
        public ArrayList<String> list;
        public String barscore;

        public ArrayList<String> getLevelAndDesc(String level, Resources resources){
            switch (level)
            {
                case "severe":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.elasticity_severe), resources.getString(R.string.elasticity_severe_desc)));
                case "moderately":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.elasticity_moderately), resources.getString(R.string.elasticity_moderately_desc)));
                case "lightly":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.elasticity_lightly), resources.getString(R.string.elasticity_lightly_desc)));
                case "none":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.elasticity_none), resources.getString(R.string.elasticity_none_desc)));
                default:
                    return new ArrayList<String>(Arrays.asList("ない","ない"));
            }
        }

        public ArrayList<String> getLevelArr(Resources resources){
            return new ArrayList<String>(Arrays.asList(resources.getString(R.string.elasticity_severe),resources.
                    getString(R.string.elasticity_moderately),resources.
                    getString(R.string.elasticity_lightly),resources.
                    getString(R.string.elasticity_none)));
        }
    }

    public class Color{
        //public String score;
        public String level;
        //public String filename;
        public String desc;
        public String info;
        public ArrayList<String> list;
        public String barscore;

        public ArrayList<String> getLevelAndDesc(String level, Resources resources){
            switch (level)
            {
                case "toubai":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.color_toubai), resources.getString(R.string.color_toubai_desc)));
                case "baixi":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.color_baixi), resources.getString(R.string.color_baixi_desc)));
                case "ziran":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.color_ziran), resources.getString(R.string.color_ziran_desc)));
                case "xiaomai":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.color_xiaomai), resources.getString(R.string.color_xiaomai_desc)));
                case "anchen":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.color_anchen), resources.getString(R.string.color_anchen_desc)));
                case "youhei":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.color_youhei), resources.getString(R.string.color_youhei_desc)));
                default:
                    return new ArrayList<String>(Arrays.asList("ない","ない"));
            }
        }

        public ArrayList<String> getLevelArr(Resources resources){
            return null;
        }
    }

    public class Blackhead{
        public String score;
        public String level;
        public String filename;
        public String desc;
        public String info;
        public ArrayList<String> list;
        public String barscore;

        public ArrayList<String> getLevelAndDesc(String level, Resources resources){
            switch (level)
            {
                case "severe":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.blackhead_severe), resources.getString(R.string.blackhead_severe_desc)));
                case "moderately":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.blackhead_moderately), resources.getString(R.string.blackhead_moderately_desc)));
                case "lightly":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.blackhead_lightly), resources.getString(R.string.blackhead_lightly_desc)));
                case "none":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.blackhead_none), resources.getString(R.string.blackhead_none_desc)));
                default:
                    return new ArrayList<String>(Arrays.asList("ない","ない"));
            }
        }

        public ArrayList<String> getLevelArr(Resources resources){
            return new ArrayList<String>(Arrays.asList(resources.getString(R.string.blackhead_severe),resources.
                    getString(R.string.blackhead_moderately),resources.
                    getString(R.string.blackhead_lightly),resources.
                    getString(R.string.blackhead_none)));
        }
    }

    public class Pore{
        public String score;
        public String level;
        public String filename;
        public String desc;
        public String info;
        public ArrayList<String> list;
        public String barscore;

        public ArrayList<String> getLevelAndDesc(String level, Resources resources){
            switch (level)
            {
                case "severe":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.pore_severe), resources.getString(R.string.pore_severe_desc)));
                case "moderately":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.pore_moderately), resources.getString(R.string.pore_moderately_desc)));
                case "lightly":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.pore_lightly), resources.getString(R.string.pore_lightly_desc)));
                case "none":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.pore_none), resources.getString(R.string.pore_none_desc)));
                default:
                    return new ArrayList<String>(Arrays.asList("ない","ない"));
            }
        }

        public ArrayList<String> getLevelArr(Resources resources){
            return new ArrayList<String>(Arrays.asList(resources.getString(R.string.pore_severe),resources.
                    getString(R.string.pore_moderately),resources.
                    getString(R.string.pore_lightly),resources.
                    getString(R.string.pore_none)));
        }
    }

    public class Roughness{
        public String score;
        public String level;
        //public String filename;
        public String desc;
        public String info;
        public ArrayList<String> list;
        public String barscore;

        public ArrayList<String> getLevelAndDesc(String level, Resources resources){
            switch (level)
            {
                case "severe":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.roughness_severe), resources.getString(R.string.roughness_severe_desc)));
                case "moderately":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.roughness_moderately), resources.getString(R.string.roughness_moderately_desc)));
                case "lightly":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.roughness_lightly), resources.getString(R.string.roughness_lightly_desc)));
                case "none":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.roughness_none), resources.getString(R.string.roughness_none_desc)));
                default:
                    return new ArrayList<String>(Arrays.asList("ない","ない"));
            }
        }

        public ArrayList<String> getLevelArr(Resources resources){
            return new ArrayList<String>(Arrays.asList(resources.getString(R.string.roughness_severe),resources.
                    getString(R.string.roughness_moderately),resources.
                    getString(R.string.roughness_lightly),resources.
                    getString(R.string.roughness_none)));
        }
    }

    public class Pigment{
        public String score;
        public String level;
        public String filename;
        public String desc;
        public String info;
        public ArrayList<String> list;
        public String barscore;

        public ArrayList<String> getLevelAndDesc(String level, Resources resources){
            switch (level)
            {
                case "severe":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.pigment_severe), resources.getString(R.string.pigment_severe_desc)));
                case "moderately":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.pigment_moderately), resources.getString(R.string.pigment_moderately_desc)));
                case "lightly":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.pigment_lightly), resources.getString(R.string.pigment_lightly_desc)));
                case "none":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.pigment_none), resources.getString(R.string.pigment_none_desc)));
                default:
                    return new ArrayList<String>(Arrays.asList("ない","ない"));
            }
        }

        public ArrayList<String> getLevelArr(Resources resources){
            return new ArrayList<String>(Arrays.asList(resources.getString(R.string.pigment_severe),resources.
                    getString(R.string.pigment_moderately),resources.
                    getString(R.string.pigment_lightly),resources.
                    getString(R.string.pigment_none)));
        }
    }

    public class Skintype{
        public String score;
        public String level;
        public String filename;
        public String desc;
        public String info;
        public ArrayList<String> list;
        public String barscore;

        public ArrayList<String> getLevelAndDesc(String level, Resources resources){
            switch (level)
            {
                case "severe":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.skin_type_severe), resources.getString(R.string.skin_type_severe_desc)));
                case "moderately":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.skin_type_moderately), resources.getString(R.string.skin_type_moderately_desc)));
                case "lightly":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.skin_type_lightly), resources.getString(R.string.skin_type_lightly_desc)));
                case "none":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.skin_type_none), resources.getString(R.string.skin_type_none_desc)));
                default:
                    return new ArrayList<String>(Arrays.asList("ない","ない"));
            }
        }

        public ArrayList<String> getLevelArr(Resources resources){
            return new ArrayList<String>(Arrays.asList(resources.getString(R.string.skin_type_severe),resources.
                    getString(R.string.skin_type_moderately),resources.
                    getString(R.string.skin_type_lightly),resources.
                    getString(R.string.skin_type_none)));
        }
    }

    public class ScalpStatus{
        public String score;
        public String level;
        public String filename;
        public String desc;
        public String info;
        public ArrayList<String> list;
        public String barscore;

        public ArrayList<String> getLevelAndDesc(String level, Resources resources){
            switch (level)
            {
                case "severe":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.scalp_status_level_lot), resources.getString(R.string.scalp_status_level_lot_desc),"25"));
                case "moderately":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.scalp_status_level_moree), resources.getString(R.string.scalp_status_level_moree_desc),"50"));
                case "lightly":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.scalp_status_level_little), resources.getString(R.string.scalp_status_level_little_desc),"75"));
                case "none":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.scalp_status_level_normal), resources.getString(R.string.scalp_status_level_normal_desc),"100"));
                default:
                    return new ArrayList<String>(Arrays.asList("ない","ない","0"));
            }
        }

        public ArrayList<String> getLevelArr(Resources resources){
            return new ArrayList<String>(Arrays.asList(resources.getString(R.string.scalp_status_level_lot),
                    resources.getString(R.string.scalp_status_level_moree),
                    resources.getString(R.string.scalp_status_level_little) ,
                    resources.getString(R.string.scalp_status_level_normal)));
        }
    }

    public class HairPore{
        public String score;
        public String level;
        public String filename;
        public String desc;
        public String info;
        public ArrayList<String> list;
        public String barscore;

        public ArrayList<String> getLevelAndDesc(String level, Resources resources){
            switch (level)
            {
                case "severe":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.hair_pore_level_severe), resources.getString(R.string.hair_pore_level_severe_desc),"25"));
                case "moderately":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.hair_pore_level_moderate), resources.getString(R.string.hair_pore_level_moderate_desc),"50"));
                case "lightly":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.hair_pore_level_mild), resources.getString(R.string.hair_pore_level_mild_desc),"75"));
                case "none":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.hair_pore_level_none), resources.getString(R.string.hair_pore_level_none_desc),"100"));
                default:
                    return new ArrayList<String>(Arrays.asList("ない","ない","0"));
            }
        }

        public ArrayList<String> getLevelArr(Resources resources){
            return new ArrayList<String>(Arrays.asList(resources.getString(R.string.hair_pore_level_severe),
                    resources.getString(R.string.hair_pore_level_moderate),
                    resources.getString(R.string.hair_pore_level_mild) ,
                    resources.getString(R.string.hair_pore_level_none)));
        }
    }

    public class ScalpKeratin{
        public String score;
        public String level;
        public String filename;
        public String desc;
        public String info;
        public ArrayList<String> list;
        public String barscore;

        public ArrayList<String> getLevelAndDesc(String level, Resources resources){
            switch (level)
            {
                case "severe":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.scalp_keratin_level_severe), resources.getString(R.string.scalp_keratin_level_severe_desc),"25"));
                case "moderately":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.scalp_keratin_level_moderate), resources.getString(R.string.scalp_keratin_level_moderate_desc),"50"));
                case "lightly":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.scalp_keratin_level_mild), resources.getString(R.string.scalp_keratin_level_mild_desc),"75"));
                case "none":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.scalp_keratin_level_none), resources.getString(R.string.scalp_keratin_level_none_desc),"100"));
                default:
                    return new ArrayList<String>(Arrays.asList("ない","ない","0"));
            }
        }

        public ArrayList<String> getLevelArr(Resources resources){
            return new ArrayList<String>(Arrays.asList(resources.getString(R.string.scalp_keratin_level_severe),
                    resources.getString(R.string.scalp_keratin_level_moderate),
                    resources.getString(R.string.scalp_keratin_level_mild) ,
                    resources.getString(R.string.scalp_keratin_level_none)));
        }
    }

    public class ScalpVessel{
        public String score;
        public String level;
        public String filename;
        public String desc;
        public String info;
        public ArrayList<String> list;
        public String barscore;

        public ArrayList<String> getLevelAndDesc(String level, Resources resources){
            switch (level)
            {
                case "severe":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.scalp_vessel_level_severe), resources.getString(R.string.scalp_vessel_level_severe_desc),"25"));
                case "moderately":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.scalp_vessel_level_moderate), resources.getString(R.string.scalp_vessel_level_moderate_desc),"50"));
                case "lightly":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.scalp_vessel_level_mild), resources.getString(R.string.scalp_vessel_level_mild_desc),"75"));
                case "none":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.scalp_vessel_level_none), resources.getString(R.string.scalp_vessel_level_none_desc),"100"));
                default:
                    return new ArrayList<String>(Arrays.asList("ない","ない","0"));
            }
        }

        public ArrayList<String> getLevelArr(Resources resources){
            return new ArrayList<String>(Arrays.asList(resources.getString(R.string.scalp_vessel_level_severe),
                    resources.getString(R.string.scalp_vessel_level_moderate),
                    resources.getString(R.string.scalp_vessel_level_mild) ,
                    resources.getString(R.string.scalp_vessel_level_none)));
        }
    }

    public class HairLost{
        public String score;
        public String level;
        public String filename;
        public String desc;
        public String info;
        public ArrayList<String> list;
        public String barscore;

        public ArrayList<String> getLevelAndDesc(String level, Resources resources){
            switch (level)
            {
                case "severe":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.hair_lost_level_severe), resources.getString(R.string.hair_lost_level_severe_desc),"25"));
                case "moderately":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.hair_lost_level_moderate), resources.getString(R.string.hair_lost_level_moderate_desc),"50"));
                case "lightly":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.hair_lost_level_mild), resources.getString(R.string.hair_lost_level_mild_desc),"75"));
                case "none":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.hair_lost_level_normal), resources.getString(R.string.hair_lost_level_normal_desc),"100"));
                default:
                    return new ArrayList<String>(Arrays.asList("ない","ない","0"));
            }
        }

        public ArrayList<String> getLevelArr(Resources resources){
            return new ArrayList<String>(Arrays.asList(resources.getString(R.string.hair_lost_level_severe),
                    resources.getString(R.string.hair_lost_level_moderate),
                    resources.getString(R.string.hair_lost_level_mild) ,
                    resources.getString(R.string.hair_lost_level_normal)));
        }
    }

    public class HairDensity{
        public String score;
        public String level;
        public String filename;
        public String desc;
        public String info;
        public ArrayList<String> list;
        public String barscore;

        public ArrayList<String> getLevelAndDesc(String level, Resources resources){
            switch (level)
            {
                case "severe":
                    return new ArrayList<String>(Arrays.asList("ない","ない","25"));
                case "moderately":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.hair_density_level_low), resources.getString(R.string.hair_density_level_low_desc),"50"));
                case "lightly":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.hair_density_level_nomarl), resources.getString(R.string.hair_density_level_nomarl_desc),"75"));
                case "none":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.hair_density_level_high), resources.getString(R.string.hair_density_level_high_desc),"100"));
                default:
                    return new ArrayList<String>(Arrays.asList("ない","ない","0"));
            }
        }

        public ArrayList<String> getLevelArr(Resources resources){
            return new ArrayList<String>(Arrays.asList("ない",resources.getString(R.string.hair_density_level_low),resources.getString(R.string.hair_density_level_nomarl) ,resources.getString(R.string.hair_density_level_high)));
        }
    }

    public class HairThickness{
        public String score;
        public String level;
        public String filename;
        public String desc;
        public String info;
        public ArrayList<String> list;
        public String barscore;

        public ArrayList<String> getLevelAndDesc(String level, Resources resources){
            switch (level)
            {
                case "thick":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.hair_thickness_level_coarse), resources.getString(R.string.hair_thickness_level_coarse_desc),"25"));
                case "medium":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.hair_thickness_level_normal), resources.getString(R.string.hair_thickness_level_normal_desc),"50"));
                case "thin":
                    return new ArrayList<String>(Arrays.asList(resources.getString(R.string.hair_thickness_level_fine), resources.getString(R.string.hair_thickness_level_fine_desc),"75"));
                case "none":
                    return new ArrayList<String>(Arrays.asList("ない","ない","100"));
                default:
                    return new ArrayList<String>(Arrays.asList("ない","ない","0"));
            }
            //return new ArrayList<String>(Arrays.asList(resources.getString(R.string.hair_thickness_level_normal), resources.getString(R.string.hair_thickness_level_normal_desc)));
        }

        public ArrayList<String> getLevelArr(Resources resources){
            return new ArrayList<String>(Arrays.asList(resources.getString(R.string.hair_thickness_level_coarse),resources.getString(R.string.hair_thickness_level_normal),resources.getString(R.string.hair_thickness_level_fine) ,"ない"));
        }
    }
}
