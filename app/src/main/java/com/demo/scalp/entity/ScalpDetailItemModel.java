package com.demo.scalp.entity;

import java.util.List;

public class ScalpDetailItemModel {
    private String titleStr;
    private String score;
    private String desStr;
    private String filename;
    private List<String> list;
    private String info;

    public ScalpDetailItemModel(String titleStr, String score, String desStr, String filename, List<String> list, String info) {
        this.titleStr = titleStr;
        this.score = score;
        this.desStr = desStr;
        this.filename = filename;
        this.list = list;
        this.info = info;
    }

    public String getTitleStr() {
        return titleStr;
    }

    public void setTitleStr(String titleStr) {
        this.titleStr = titleStr;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getDesStr() {
        return desStr;
    }

    public void setDesStr(String desStr) {
        this.desStr = desStr;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
