package com.demo.scalp.Enum;

public enum EnumLight {
    WhiteLight("11"),
    PolarizationLight("12"),
    UvLight("13");

    private final String value;

    // 构造器默认也只能是private, 从而保证构造函数只能在内部使用
    EnumLight(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
