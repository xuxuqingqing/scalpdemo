package com.demo.scalp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.demo.scalp.R;
import com.demo.scalp.database.DBUtil;
import com.demo.scalp.entity.HistoryItemModel;
import com.demo.scalp.entity.ShowEntity;
import com.demo.scalp.utils.AutoUtils;
import com.google.gson.Gson;

import java.util.List;
public class HistoryAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private List<HistoryItemModel> info;
    private Gson gson;
    private DBUtil dbUtil;
    private boolean showSelected;
    private CallBack callBack;

    public HistoryAdapter(Context context, List<HistoryItemModel> info,CallBack callBack){
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.info=info;
        this.callBack = callBack;
        gson=new Gson();
        dbUtil = new DBUtil(context);
    }
    @Override
    public int getCount() {
        return info.size();
    }

    @Override
    public HistoryItemModel getItem(int i) {
        return info.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View contentView, ViewGroup parent) {
        final ViewHolder holder;
        if (contentView == null) {
            contentView = inflater.inflate(R.layout.scalp_history_item, null);
            holder = new ViewHolder(contentView);
            AutoUtils.auto(contentView);
            contentView.setTag(holder);
        } else {
            holder = (ViewHolder) contentView.getTag();
        }
        holder.timeTv.setText(getItem(position).getAddTime());
        ShowEntity showEntity = gson.fromJson(getItem(position).getContent(),ShowEntity.class);
        holder.scoreTv.setText(showEntity.score);
        if (showSelected){
            holder.deleteSelected.setVisibility(View.VISIBLE);
        }else{
            holder.deleteSelected.setVisibility(View.GONE);
        }
        if (getItem(position).isSelected()){
            holder.deleteSelected.setImageResource(R.mipmap.history_selected);
        }else{
            holder.deleteSelected.setImageResource(R.mipmap.history_unselected);
        }
        holder.deleteSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getItem(position).isSelected()){
                    holder.deleteSelected.setImageResource(R.mipmap.history_unselected);
                    info.get(position).setSelected(false);
                    callBack.refreshDeleteState(getItem(position).getId(),"remove");
                }else{
                    holder.deleteSelected.setImageResource(R.mipmap.history_selected);
                    info.get(position).setSelected(true);
                    callBack.refreshDeleteState(getItem(position).getId(),"add");
                }

            }
        });
        return contentView;
    }
    class ViewHolder {
       ImageView deleteSelected;
       TextView scoreTv;
       TextView timeTv;
       View dividerView;

        public ViewHolder(View itemView) {
            scoreTv = itemView.findViewById(R.id.scalp_history_score_tv);
            timeTv = itemView.findViewById(R.id.scalp_history_time_tv);
            dividerView = itemView.findViewById(R.id.scalp_history_divider);
            deleteSelected = itemView.findViewById(R.id.scalp_history_delete_selected);
        }
    }

    public void setShowSelected(boolean showSelected) {
        this.showSelected = showSelected;
    }
    public interface CallBack{
        void refreshDeleteState(int id,String doType);
    }
}
