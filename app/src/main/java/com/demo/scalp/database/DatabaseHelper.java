package com.demo.scalp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String name = "lm_study"; //database name

    private static final int version = 2; //database version

    public DatabaseHelper(Context context) {
        super(context, name, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // create mistake table
        db.execSQL("CREATE TABLE IF NOT EXISTS sd_history (id INTEGER primary key autoincrement,add_time varchar(30),content TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        /*if (oldVersion==1 && newVersion==2) {//database update
            db.execSQL("ALTER TABLE lm_mistake ADD phone TEXT;");
        }*/
    }
}
