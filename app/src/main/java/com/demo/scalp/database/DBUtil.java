package com.demo.scalp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.demo.scalp.entity.HistoryItemModel;
import java.util.ArrayList;
import java.util.List;


public class DBUtil {
    private SQLiteDatabase sqLiteDatabase;
    public DBUtil(Context context) {
        sqLiteDatabase=new DatabaseHelper(context).getWritableDatabase();

    }

    /**
     * insert history item
     * @param baseQueryModel
     */
    public long insert(HistoryItemModel baseQueryModel){
        if (baseQueryModel.getContent()==null||"".equals(baseQueryModel.getContent())){
            return -1;
        }
        ContentValues cv = new ContentValues();
        cv.put("content",baseQueryModel.getContent());
        cv.put("add_time",baseQueryModel.getAddTime());
        return sqLiteDatabase.insert("sd_history",null,cv);
    }
    /**
     * query history all data
     * @return
     */
    public List<HistoryItemModel> getAll(){
        List<HistoryItemModel> data=new ArrayList<>();
        String tableName="sd_history";
        StringBuilder buf = new StringBuilder("SELECT * FROM "+ tableName);
        Cursor cursor = sqLiteDatabase.rawQuery(buf.toString(),null);
        if (cursor.getCount()>=0){
            while(cursor.moveToNext()){
                HistoryItemModel baseQueryModel=new HistoryItemModel();
                baseQueryModel.setId(cursor.getInt(0));
                baseQueryModel.setAddTime(cursor.getString(1));
                baseQueryModel.setContent(cursor.getString(2));
                data.add(baseQueryModel);
            }
        }
        if(cursor!=null){
        cursor.close();
        }
        return data;

    }

    /**
     * delete History item By id
     * @return
     */
    public boolean deleteHistoryById(List<Integer> ids){
        String[] temp=new String[ids.size()];
        String flg="id=?";
        for (int i=0;i<ids.size();i++){
            temp[i]=String.valueOf(ids.get(i));
            if (i>0){
                flg+=" or id=?";
            }
        }

        int result=sqLiteDatabase.delete("sd_history",flg,temp);
        if (result>0){
            return true;
        }else{
            return false;
        }
    }

}
