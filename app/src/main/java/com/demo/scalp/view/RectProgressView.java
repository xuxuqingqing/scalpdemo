package com.demo.scalp.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.demo.scalp.R;

public class RectProgressView extends View {
    private Paint mPaint = null ;
    private int max=100;
    private int RProgress=0;
    private int myColor;

    public RectProgressView(Context context) {
        this(context, null);
    }

    public RectProgressView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RectProgressView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs);
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.RectProgressBar); //与属性名称一致
        max = array.getInteger(R.styleable.RectProgressBar_max, 100);//第一个是传递参数，第二个是默认值
        RProgress = array.getInteger(R.styleable.RectProgressBar_rProgress, 0);
        myColor=array.getColor(R.styleable.RectProgressBar_pColor, Color.BLUE);
    }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Point left_top=new Point(0,0);
        Point right_bottom=new Point(getWidth(),getHeight());
        double rate=(double)RProgress/(double)max;
        drawProgressBar(canvas, left_top, right_bottom, rate);
    }

    public void setRProgress(int rProgress) {
        this.RProgress = rProgress;
        invalidate();
    }
    private void drawProgressBar(Canvas canvas, Point left_top, Point right_bottom, double rate){
        int width=0;
        int rad=10;
        //mPaint.setColor(Color.BLACK);//画边框
        //mPaint.setStyle(Paint.Style.STROKE);//设置空心
        //mPaint.setStrokeWidth(width);
        //RectF rectF = new RectF(left_top.x,left_top.y,right_bottom.x,right_bottom.y);
        //canvas.drawRoundRect(rectF, rad, rad, mPaint);

        mPaint.setColor(myColor);//画progress bar
        mPaint.setStyle(Paint.Style.FILL);
        int x_end=(int)(right_bottom.x*rate);
        RectF rectF2 = new RectF(left_top.x+width,left_top.y+width,x_end-width,right_bottom.y-width);
        canvas.drawRoundRect(rectF2, rad, rad, mPaint);
    }

}
