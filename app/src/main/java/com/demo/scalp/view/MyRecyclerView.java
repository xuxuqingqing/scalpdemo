package com.demo.scalp.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.demo.scalp.binding.myrecycleview.BindAdapter;
import com.demo.scalp.binding.myrecycleview.OnRecyclerItemClickListener;

public class MyRecyclerView extends RecyclerView {
    public MyRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public void setOnRecyclerItemClickListener(OnRecyclerItemClickListener onRecyclerItemClickListener) {

        BindAdapter bindAdapter = getBindAdapter();
        if (bindAdapter != null && onRecyclerItemClickListener != null) {
            bindAdapter.setItemOneClickListener(onRecyclerItemClickListener);
        }

    }

    private BindAdapter getBindAdapter() {
        if (getAdapter() instanceof BindAdapter) {
            return (BindAdapter) getAdapter();
        }

        return null;
    }
}
