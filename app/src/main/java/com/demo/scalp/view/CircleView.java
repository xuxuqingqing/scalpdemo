package com.demo.scalp.view;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.OvershootInterpolator;

import com.demo.scalp.R;

public class CircleView extends View {
    //    定义画笔
    Paint paint;
    private int myColor;

    private Paint mBackPaint, mProgPaint;   // 绘制画笔
    private RectF mRectF;       // 绘制区域
    //private int[] mColorArray;  // 圆环渐变色
    //private int mProgress;      // 圆环进度(0-100)
    //private int dTime;

    public CircleView(Context context) {
        this(context, null);
    }

    public CircleView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        @SuppressLint("Recycle")
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CircleView);

        // 初始化背景圆环画笔
        mBackPaint = new Paint();
        mBackPaint.setStyle(Paint.Style.FILL);    // 只描边，不填充
        mBackPaint.setStrokeCap(Paint.Cap.ROUND);   // 设置圆角
        mBackPaint.setAntiAlias(true);              // 设置抗锯齿
        mBackPaint.setDither(true);                 // 设置抖动
        //mBackPaint.setStrokeWidth(typedArray.getDimension(R.styleable.CircularProgressView_backWidth, 5));
        mBackPaint.setColor(typedArray.getColor(R.styleable.CircleView_bgColor, Color.RED));


        // 初始化进度圆环画笔
        //mProgPaint = new Paint();
       // mProgPaint.setStyle(Paint.Style.STROKE);    // 只描边，不填充
        //mProgPaint.setStrokeCap(Paint.Cap.ROUND);   // 设置圆角
        //mProgPaint.setAntiAlias(true);              // 设置抗锯齿
        //mProgPaint.setDither(true);                 // 设置抖动
       // mProgPaint.setStrokeWidth(typedArray.getDimension(R.styleable.CircularProgressView_progWidth, 10));
       // mProgPaint.setColor(typedArray.getColor(R.styleable.CircularProgressView_progColor, Color.BLUE));

        // 初始化进度圆环渐变色
        //int firstColor = typedArray.getColor(R.styleable.CircularProgressView_progFirstColor, -1);
        //if (startColor != -1 && firstColor != -1) mColorArray = new int[]{startColor, firstColor};
       // else mColorArray = null;

        // 初始化进度
        //mProgress = typedArray.getInteger(R.styleable.CircularProgressView_progress, 0);
        //dTime=typedArray.getInteger(R.styleable.CircularProgressView_dTime, 0);
        typedArray.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int viewWide = getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
        int viewHigh = getMeasuredHeight() - getPaddingTop() - getPaddingBottom();
        int mRectLength = (int) ((viewWide > viewHigh ? viewHigh : viewWide) - mBackPaint.getStrokeWidth());
        int mRectL = getPaddingLeft() + (viewWide - mRectLength) / 2;
        int mRectT = getPaddingTop() + (viewHigh - mRectLength) / 2;
        mRectF = new RectF(mRectL, mRectT, mRectL + mRectLength, mRectT + mRectLength);

        // 设置进度圆环渐变色
        //if (mColorArray != null && mColorArray.length > 1)
           // mProgPaint.setShader(new LinearGradient(0, 0, 0, getMeasuredWidth(), mColorArray, null, Shader.TileMode.MIRROR));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawArc(mRectF, 0, 360, false, mBackPaint);
        //canvas.drawArc(mRectF, 275, 360 * mProgress / 100, false, mProgPaint);

    }
}
