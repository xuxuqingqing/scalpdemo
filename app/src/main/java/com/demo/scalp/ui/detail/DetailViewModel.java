package com.demo.scalp.ui.detail;

import android.app.Application;
import android.content.Intent;
import android.content.res.Resources;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.databinding.ObservableList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.demo.scalp.BR;
import com.demo.scalp.Enum.EnumScalp;
import com.demo.scalp.R;
import com.demo.scalp.data.DemoRepository;
import com.demo.scalp.entity.DetailEntity;
import com.demo.scalp.entity.ShowEntity;
import com.demo.scalp.ui.scalpmain.MainActivity;

import java.util.ArrayList;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableObserver;
import me.goldze.mvvmhabit.base.BaseViewModel;
import me.goldze.mvvmhabit.binding.command.BindingAction;
import me.goldze.mvvmhabit.binding.command.BindingCommand;
import me.goldze.mvvmhabit.utils.RxUtils;
import me.goldze.mvvmhabit.utils.ToastUtils;
import me.tatarka.bindingcollectionadapter2.ItemBinding;

public class DetailViewModel extends BaseViewModel<DemoRepository> {
    public String tActivity;
    public final ObservableList<String> items = new ObservableArrayList<>();
    public final ItemBinding<String> itemBinding = ItemBinding.of(BR.desItem, R.layout.scalp_detail_des_item);
    public ObservableField<ShowEntity> entity = new ObservableField<ShowEntity>();
    public ObservableInt item1Visi = new ObservableInt(View.INVISIBLE);
    public ObservableInt item2Visi = new ObservableInt(View.INVISIBLE);
    public ObservableInt item3Visi = new ObservableInt(View.INVISIBLE);
    public ObservableInt item4Visi = new ObservableInt(View.INVISIBLE);
    public ObservableInt item5Visi = new ObservableInt(View.INVISIBLE);
    public ObservableInt item6Visi = new ObservableInt(View.INVISIBLE);
    public ObservableInt item7Visi = new ObservableInt(View.INVISIBLE);
    public ObservableInt item8Visi = new ObservableInt(View.INVISIBLE);
    public ObservableInt item9Visi = new ObservableInt(View.INVISIBLE);
    public ObservableInt item10Visi = new ObservableInt(View.INVISIBLE);
    public ObservableInt item11Visi = new ObservableInt(View.INVISIBLE);
    public ObservableInt item12Visi = new ObservableInt(View.INVISIBLE);
    public ObservableInt item13Visi = new ObservableInt(View.INVISIBLE);
    public ObservableInt item14Visi = new ObservableInt(View.INVISIBLE);
    public ObservableInt item15Visi = new ObservableInt(View.INVISIBLE);
    public ObservableInt item16Visi = new ObservableInt(View.INVISIBLE);
    public ObservableField<int[]> instrucArr = new ObservableField<>(new int[]{View.INVISIBLE,View.INVISIBLE,View.INVISIBLE,View.INVISIBLE,View.INVISIBLE,View.INVISIBLE,View.INVISIBLE}) ;
    public DetailViewModel(@NonNull Application application, DemoRepository repository) {
        super(application, repository);
    }
    public BindingCommand backIcon = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            if (tActivity.equals("main")){
                startActivity(MainActivity.class);
            }
            finish();
        }
    });

    public BindingCommand instrucClick = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            if(item1Visi.get()== View.VISIBLE){
                item1Visi.set(View.INVISIBLE);
            }else{
                item1Visi.set(View.VISIBLE);
            }
        }
    });
    public BindingCommand closeDesClick1 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            item1Visi.set(View.INVISIBLE);
        }
    });
    public BindingCommand closeDesClick2 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            item2Visi.set(View.INVISIBLE);
        }
    });
    public BindingCommand closeDesClick3 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            item3Visi.set(View.INVISIBLE);
        }
    });
    public BindingCommand closeDesClick4 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            item4Visi.set(View.INVISIBLE);
        }
    });
    public BindingCommand closeDesClick5 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            item5Visi.set(View.INVISIBLE);
        }
    });
    public BindingCommand closeDesClick6 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            item6Visi.set(View.INVISIBLE);
        }
    });
    public BindingCommand closeDesClick7 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            item7Visi.set(View.INVISIBLE);
        }
    });
    public BindingCommand closeDesClick8 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            item8Visi.set(View.INVISIBLE);
        }
    });
    public BindingCommand closeDesClick9 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            item9Visi.set(View.INVISIBLE);
        }
    });
    public BindingCommand closeDesClick10 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            item10Visi.set(View.INVISIBLE);
        }
    });
    public BindingCommand closeDesClick11 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            item11Visi.set(View.INVISIBLE);
        }
    });
    public BindingCommand closeDesClick12 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            item12Visi.set(View.INVISIBLE);
        }
    });
    public BindingCommand closeDesClick13 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            item13Visi.set(View.INVISIBLE);
        }
    });
    public BindingCommand closeDesClick14 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            item14Visi.set(View.INVISIBLE);
        }
    });
    public BindingCommand closeDesClick15 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            item15Visi.set(View.INVISIBLE);
        }
    });
    public BindingCommand closeDesClick16 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            item16Visi.set(View.INVISIBLE);
        }
    });
    public BindingCommand instrucClick1 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            if(item2Visi.get()== View.VISIBLE){
                item2Visi.set(View.INVISIBLE);
            }else{
                item2Visi.set(View.VISIBLE);
            }
        }
    });
    public BindingCommand instrucClick2 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            if(item3Visi.get()== View.VISIBLE){
                item3Visi.set(View.INVISIBLE);
            }else{
                item3Visi.set(View.VISIBLE);
            }
        }
    });
    public BindingCommand instrucClick3 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            if(item4Visi.get()== View.VISIBLE){
                item4Visi.set(View.INVISIBLE);
            }else{
                item4Visi.set(View.VISIBLE);
            }
        }
    });
    public BindingCommand instrucClick4 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            //iClick(4);
            if(item5Visi.get()== View.VISIBLE){
                item5Visi.set(View.INVISIBLE);
            }else{
                item5Visi.set(View.VISIBLE);
            }
        }
    });
    public BindingCommand instrucClick5 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            //iClick(5);
            if(item6Visi.get()== View.VISIBLE){
                item6Visi.set(View.INVISIBLE);
            }else{
                item6Visi.set(View.VISIBLE);
            }
        }
    });
    public BindingCommand instrucClick6 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
           // iClick(6);
            if(item7Visi.get()== View.VISIBLE){
                item7Visi.set(View.INVISIBLE);
            }else{
                item7Visi.set(View.VISIBLE);
            }
        }
    });

    public BindingCommand instrucClick7 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            // iClick(6);
            if(item8Visi.get()== View.VISIBLE){
                item8Visi.set(View.INVISIBLE);
            }else{
                item8Visi.set(View.VISIBLE);
            }
        }
    });

    public BindingCommand instrucClick8 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            // iClick(6);
            if(item9Visi.get()== View.VISIBLE){
                item9Visi.set(View.INVISIBLE);
            }else{
                item9Visi.set(View.VISIBLE);
            }
        }
    });

    public BindingCommand instrucClick9 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            // iClick(6);
            if(item10Visi.get()== View.VISIBLE){
                item10Visi.set(View.INVISIBLE);
            }else{
                item10Visi.set(View.VISIBLE);
            }
        }
    });

    public BindingCommand instrucClick10 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            // iClick(6);
            if(item11Visi.get()== View.VISIBLE){
                item11Visi.set(View.INVISIBLE);
            }else{
                item11Visi.set(View.VISIBLE);
            }
        }
    });

    public BindingCommand instrucClick11 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            // iClick(6);
            if(item12Visi.get()== View.VISIBLE){
                item12Visi.set(View.INVISIBLE);
            }else{
                item12Visi.set(View.VISIBLE);
            }
        }
    });

    public BindingCommand instrucClick12 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            // iClick(6);
            if(item13Visi.get()== View.VISIBLE){
                item13Visi.set(View.INVISIBLE);
            }else{
                item13Visi.set(View.VISIBLE);
            }
        }
    });

    public BindingCommand instrucClick13 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            // iClick(6);
            if(item14Visi.get()== View.VISIBLE){
                item14Visi.set(View.INVISIBLE);
            }else{
                item14Visi.set(View.VISIBLE);
            }
        }
    });

    public BindingCommand instrucClick14 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            // iClick(6);
            if(item15Visi.get()== View.VISIBLE){
                item15Visi.set(View.INVISIBLE);
            }else{
                item15Visi.set(View.VISIBLE);
            }
        }
    });

    public BindingCommand instrucClick15 = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            // iClick(6);
            if(item16Visi.get()== View.VISIBLE){
                item16Visi.set(View.INVISIBLE);
            }else{
                item16Visi.set(View.VISIBLE);
            }
        }
    });

    /**
     * 点击说明图标
     * @param index
     */
    /*private void iClick(int index){
        if(instrucArr.get()[index]== View.VISIBLE){
            int[] temp = instrucArr.get();
            temp[index]=View.INVISIBLE;
            instrucArr.set(temp);
        }else{
            int[] temp = instrucArr.get();
            temp[index]=View.VISIBLE;
            instrucArr.set(temp);
        }
    }*/
}
