package com.demo.scalp.ui.camera;

import android.app.Application;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;

import com.demo.scalp.Enum.EnumLight;
import com.demo.scalp.data.DemoRepository;
import com.demo.scalp.ui.scalpmain.MainActivity;

import me.goldze.mvvmhabit.base.BaseViewModel;
import me.goldze.mvvmhabit.binding.command.BindingAction;
import me.goldze.mvvmhabit.binding.command.BindingCommand;

public class CameraViewModel extends BaseViewModel<DemoRepository> {
    public ObservableField<String> img1 = new ObservableField("");
    public ObservableField<String> img2 = new ObservableField("");
    public ObservableField<String> img3 = new ObservableField("");
    public ObservableBoolean isClick = new ObservableBoolean(true);
    public CameraViewModel(@NonNull Application application, DemoRepository model) {
        super(application, model);
    }
    public BindingCommand backIcon = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
        if(isClick.get()){
            finish();
        }
        }
    });
}

