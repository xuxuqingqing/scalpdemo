package com.demo.scalp.ui.detail;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.demo.scalp.BR;
import com.demo.scalp.R;
import com.demo.scalp.app.AppViewModelFactory;
import com.demo.scalp.database.DBUtil;
import com.demo.scalp.databinding.ScalpDetailBinding;
import com.demo.scalp.entity.HistoryItemModel;
import com.demo.scalp.entity.ShowEntity;
import com.demo.scalp.ui.base.MyBaseActivity;
import com.demo.scalp.ui.scalpmain.MainActivity;
import com.demo.scalp.view.RadarView;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class DetailActivity extends MyBaseActivity<ScalpDetailBinding,DetailViewModel> {
    private HistoryItemModel historyItemModel;
    @Override
    public int initContentView(Bundle savedInstanceState) {
        return R.layout.scalp_detail;
    }

    @Override
    public int initVariableId() {
        return BR.viewModel;
    }
    public final static int INSERT_HISTORY=1;
    private DBUtil dbUtil;
    Handler myHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case INSERT_HISTORY:
                    if (historyItemModel!=null&&dbUtil!=null)
                    dbUtil.insert(historyItemModel);
                    break;
            }
        }
    };
    @Override
    public DetailViewModel initViewModel() {
        //使用自定义的ViewModelFactory来创建ViewModel，如果不重写该方法，则默认会调用LoginViewModel(@NonNull Application application)构造方法
        AppViewModelFactory factory = AppViewModelFactory.getInstance(getApplication());
        return ViewModelProviders.of(this, factory).get(DetailViewModel.class);
    }
    @Override
    public void initData() {
        super.initData();
        Bundle bundle = this.getIntent().getExtras();
        ShowEntity showEntity = new Gson().fromJson(bundle.getString("entity"),ShowEntity.class);
        viewModel.entity.set(showEntity);
        ((RadarView)findViewById(R.id.scalp_detail_rv)).setValue(this,showEntity);
        viewModel.tActivity=bundle.getString("activity");
        if (bundle.getString("activity").equals("main")){
            dbUtil = new DBUtil(this);
            historyItemModel=new HistoryItemModel();
            historyItemModel.setContent(bundle.getString("entity"));
            SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");
            String curTime=format.format(new Date());
            historyItemModel.setAddTime(curTime);
            Message msg = new Message();
            msg.what=INSERT_HISTORY;
            myHandler.sendMessage(msg);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (viewModel.tActivity.equals("main")){
            startActivity(MainActivity.class);
        }
        finish();
    }
}
