package com.demo.scalp.ui.history;

import android.app.Dialog;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.demo.scalp.BR;
import com.demo.scalp.MyCustomDialog;
import com.demo.scalp.R;
import com.demo.scalp.adapter.HistoryAdapter;
import com.demo.scalp.app.AppViewModelFactory;
import com.demo.scalp.database.DBUtil;
import com.demo.scalp.databinding.ScalpHistoryBinding;
import com.demo.scalp.entity.HistoryItemModel;
import com.demo.scalp.ui.base.MyBaseActivity;
import com.demo.scalp.ui.detail.DetailActivity;

import java.util.ArrayList;
import java.util.List;

import me.goldze.mvvmhabit.utils.ConvertUtils;
import me.goldze.mvvmhabit.utils.ToastUtils;

public class HistoryActivity extends MyBaseActivity<ScalpHistoryBinding, HistoryViewModel> implements HistoryAdapter.CallBack {
    private SwipeMenuListView listView;
    private List<HistoryItemModel> datas;
    private  HistoryAdapter historyAdapter;
    private TextView deleteTv;
    private List<Integer> ids;
    private DBUtil dbUtil;
    final static int DELETE_ONE=1;
    final static int DELETE_MORE=2;
    @Override
    public int initContentView(Bundle savedInstanceState) {
        return R.layout.scalp_history;
    }

    @Override
    public int initVariableId() {
        return BR.viewModel;
    }

    @Override
    public HistoryViewModel initViewModel() {
        //使用自定义的ViewModelFactory来创建ViewModel，如果不重写该方法，则默认会调用LoginViewModel(@NonNull Application application)构造方法
        AppViewModelFactory factory = AppViewModelFactory.getInstance(getApplication());
        return ViewModelProviders.of(this, factory).get(HistoryViewModel.class);
    }
    @Override
    public void initData() {
        super.initData();
        listView = findViewById(R.id.scalp_history_lv);
        dbUtil = new DBUtil(this);
        datas = dbUtil.getAll();
        if (datas!=null && datas.size()>0){
            ids=new ArrayList<>();
            viewModel.noData.set(View.INVISIBLE);
            viewModel.backV.set(View.VISIBLE);
            viewModel.moreV.set(View.VISIBLE);
            historyAdapter = new HistoryAdapter(this,datas,this);
            listView.setAdapter(historyAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Bundle bundle = new Bundle();
                    bundle.putString("entity", datas.get(i).getContent());
                    bundle.putString("activity","history");
                    startActivity(DetailActivity.class,bundle);
                }
            });
            initListView();
        }
        deleteTv=(TextView) findViewById(R.id.scalp_history_delete_start_tv);
        //删除部分
        findViewById(R.id.scalp_history_delete_part).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (historyAdapter!=null){
                    //viewModel.backV.set(View.INVISIBLE);
                    viewModel.cancelBtn.set(View.VISIBLE);
                    viewModel.deleteS.set(View.VISIBLE);
                    deleteTv.setTextColor(getResources().getColor(R.color.textColorHint));
                    deleteTv.setClickable(false);
                    viewModel.deletePow.set(View.INVISIBLE);
                    viewModel.moreV.set(View.INVISIBLE);
                    viewModel.backV.set(View.INVISIBLE);
                    historyAdapter.setShowSelected(true);
                    historyAdapter.notifyDataSetChanged();
                }
            }
        });
        //删除所有
        findViewById(R.id.scalp_history_delete_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if (historyAdapter!=null){
                   //viewModel.backV.set(View.INVISIBLE);
                   viewModel.cancelBtn.set(View.VISIBLE);
                   viewModel.deleteS.set(View.VISIBLE);
                   deleteTv.setClickable(true);
                   deleteTv.setTextColor(getResources().getColor(R.color.colorPrimary));
                   viewModel.deletePow.set(View.INVISIBLE);
                   viewModel.moreV.set(View.INVISIBLE);
                   viewModel.backV.set(View.INVISIBLE);
                   ids.clear();
                   for (int i=0;i<datas.size();i++){
                       datas.get(i).setSelected(true);
                       ids.add(datas.get(i).getId());
                   }
                   historyAdapter.setShowSelected(true);
                   historyAdapter.notifyDataSetChanged();
               }
            }
        });
        //取消删除
        findViewById(R.id.scalp_history_cancel_tv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (historyAdapter!=null){
                    //viewModel.backV.set(View.INVISIBLE);
                    viewModel.cancelBtn.set(View.INVISIBLE);
                    viewModel.deleteS.set(View.INVISIBLE);
                    viewModel.moreV.set(View.VISIBLE);
                    viewModel.backV.set(View.VISIBLE);
                    for (int i=0;i<datas.size();i++){
                        datas.get(i).setSelected(false);
                    }
                    ids.clear();
                    historyAdapter.setShowSelected(false);
                    historyAdapter.notifyDataSetChanged();
                }
            }
        });
        deleteTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ids.size()>0){
                    showCustomDialog(DELETE_MORE,0);
                }
            }
        });

    }
    private void initListView(){
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.rgb(255, 104,
                        155)));
                // set item width
                openItem.setWidth(ConvertUtils.dp2px(90));
                // set item title
                openItem.setTitle("削除");
                // set item title fontsize
                openItem.setTitleSize(18);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);

                // create "delete" item
                //SwipeMenuItem deleteItem = new SwipeMenuItem(getApplicationContext());
                // set item background
                //deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,0x3F, 0x25)));
                // set item width
                //deleteItem.setWidth(ConvertUtils.dp2px(90));
                // set a icon
               // deleteItem.setIcon(R.mipmap.delete_icon);
                // add to menu
               // menu.addMenuItem(deleteItem);
            }
        };

// set creator
        listView.setMenuCreator(creator);
        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        // open
                        //ToastUtils.showShort(position+"");
                        showCustomDialog(DELETE_ONE,datas.get(position).getId());
                        break;
                    case 1:
                        // delete
                        break;
                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });
        listView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
    }

    /**
     *
     * @param deleteType 1单条 2 多条
     * @param id
     */
    private void showCustomDialog(final int deleteType, final int id){
        final MyCustomDialog customDialog = new MyCustomDialog(this);
        customDialog.setTitle("確認");
        customDialog.setMessage("診断履歴を削除してもよろしいですか？");
        customDialog.setYesOnclickListener("削除", new MyCustomDialog.onYesOnclickListener() {
            @Override
            public void onYesClick() {
                //这里是确定的逻辑代码，别忘了点击确定后关闭对话框
                customDialog.dismiss();
                if (deleteType==DELETE_ONE){
                    ids.clear();
                    ids.add(id);
                }
                boolean result=dbUtil.deleteHistoryById(ids);
                if (result){
                    //ToastUtils.showShort("success");
                    for(int i=0;i<ids.size();i++){
                        for (int j=0;j<datas.size();j++){
                            if(ids.get(i)==datas.get(j).getId()){
                                datas.remove(j);
                            }
                        }

                    }
                    ids.clear();
                    historyAdapter.notifyDataSetChanged();
                    if (datas.size()==0){
                        viewModel.cancelBtn.set(View.INVISIBLE);
                        viewModel.deleteS.set(View.INVISIBLE);
                        viewModel.deletePow.set(View.INVISIBLE);
                        viewModel.moreV.set(View.INVISIBLE);
                        viewModel.backV.set(View.VISIBLE);
                        viewModel.noData.set(View.VISIBLE);
                    }
                }else{
                    ToastUtils.showShort("fail");
                }
            }
        });
        customDialog.setNoOnclickListener("キャンセル", new MyCustomDialog.onNoOnclickListener() {
            @Override
            public void onNoClick() {
                customDialog.dismiss();
            }
        });
        customDialog.show();
    }
    @Override
    public void refreshDeleteState(int id,String doType) {
        switch (doType){
            case "remove":
               for(int i=0;i<ids.size();i++){
                   if(id==ids.get(i)){
                       ids.remove(i);
                   }
               }
               if (ids.size()==0){
                   deleteTv.setTextColor(getResources().getColor(R.color.textColorHint));
                   deleteTv.setClickable(false);
               }
                break;
            case "add":
                ids.add(id);
                deleteTv.setClickable(true);
                deleteTv.setTextColor(getResources().getColor(R.color.colorPrimary));
                break;
        }
    }
}
