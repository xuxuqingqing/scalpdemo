package com.demo.scalp.ui.camera;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.ObservableField;
import android.hardware.usb.UsbDevice;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.view.ViewTreeObserver;

import com.demo.scalp.BR;
import com.demo.scalp.Enum.EnumLight;
import com.demo.scalp.R;
import com.demo.scalp.app.AppViewModelFactory;
import com.demo.scalp.databinding.CameraActivityBinding;
import com.demo.scalp.ui.base.MyBaseActivity;
import com.demo.scalp.ui.detail.DetailViewModel;
import com.demo.scalp.ui.scalpmain.MainActivity;
import com.lgh.uvccamera.UVCCameraProxy;
import com.lgh.uvccamera.bean.PicturePath;
import com.lgh.uvccamera.callback.ConnectCallback;
import com.lgh.uvccamera.callback.PhotographCallback;
import com.lgh.uvccamera.callback.PictureCallback;
import com.lgh.uvccamera.callback.PreviewCallback;

import java.util.ArrayList;
import java.util.HashMap;

import me.nereo.multi_image_selector.MultiImageSelector;

public class CameraActivity extends MyBaseActivity<CameraActivityBinding,CameraViewModel> {
    @Override
    public int initContentView(Bundle savedInstanceState) {
        return R.layout.camera_activity;
    }

    @Override
    public int initVariableId() {
        return BR.viewModel;
    }

    @Override
    public CameraViewModel initViewModel() {
        //使用自定义的ViewModelFactory来创建ViewModel，如果不重写该方法，则默认会调用LoginViewModel(@NonNull Application application)构造方法
        AppViewModelFactory factory = AppViewModelFactory.getInstance(getApplication());
        return ViewModelProviders.of(this, factory).get(CameraViewModel.class);
    }

    @Override
    public void initData() {
        super.initData();
        initUVCCamera();

        findViewById(R.id.camera_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TakePic();
            }
        });

        findViewById(R.id.camera_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.isEmpty(viewModel.img1.get()) || TextUtils.isEmpty(viewModel.img2.get()) || TextUtils.isEmpty(viewModel.img3.get()) || !viewModel.isClick.get())
                {
                    return;
                }
                Intent intent = getIntent();
                ArrayList<String> stringList = new ArrayList<String>();
                stringList.add(viewModel.img1.get());
                stringList.add(viewModel.img2.get());
                stringList.add(viewModel.img3.get());
                intent.putStringArrayListExtra("ListString", stringList);
                setResult(RESULT_OK,intent);
                finish();
            }
        });
    }
    Handler myHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Message innerMsg = new Message();
            switch (msg.what){
                case 0:
                    setLight(EnumLight.WhiteLight);
                    innerMsg.what = 1;
                    myHandler.sendMessageDelayed(innerMsg,750);
                    break;
                case 1:
                    mUVCCamera.takePicture("image1.jpg");
                    break;
                case 2:
                    setLight(EnumLight.PolarizationLight);
                    innerMsg.what = 3;
                    myHandler.sendMessageDelayed(innerMsg,750);
                    break;
                case 3:
                    mUVCCamera.takePicture("image2.jpg");
                    break;
                case 4:
                    setLight(EnumLight.UvLight);
                    innerMsg.what = 5;
                    myHandler.sendMessageDelayed(innerMsg,750);
                    break;
                case 5:
                    mUVCCamera.takePicture("image3.jpg");
                    break;
                case 6:
                    setLight(EnumLight.WhiteLight);
                    break;
            }
        }
    };

    private UVCCameraProxy mUVCCamera;
    private String path1;
    private TextureView textureView;
    private int index;


    private void TakePic(){
        if(!viewModel.isClick.get())
        {
            return;
        }
        viewModel.img1.set("");
        viewModel.img2.set("");
        viewModel.img3.set("");
        viewModel.isClick.set(false);
        index = 0;
        Message innerMsg = new Message();
        innerMsg.what = 0;
        myHandler.sendMessage(innerMsg);
    }

    private void initUVCCamera() {
        textureView=(TextureView) findViewById(R.id.camera_activity_texture);
        mUVCCamera = new UVCCameraProxy(this);
        // 已有默认配置，不需要可以不设置
        mUVCCamera.getConfig()
                .isDebug(true)
                .setPicturePath(PicturePath.SDCARD)
                .setDirName("uvccamera")
                .setProductId(0)
                .setVendorId(0);
        mUVCCamera.setPreviewTexture(textureView);
//        mUVCCamera.setPreviewSurface(mSurfaceView);
//        mUVCCamera.registerReceiver();

        Log.e("camera","start");
        mUVCCamera.setConnectCallback(new ConnectCallback() {
            @Override
            public void onAttached(UsbDevice usbDevice) {
                Log.e("camera","connect");
                findViewById(R.id.camera_disconnect).setVisibility(View.GONE);
                mUVCCamera.requestPermission(usbDevice);
            }

            @Override
            public void onGranted(UsbDevice usbDevice, boolean granted) {
                if (granted) {
                    mUVCCamera.connectDevice(usbDevice);
                }
            }

            @Override
            public void onConnected(UsbDevice usbDevice) {
                mUVCCamera.openCamera();
            }

            @Override
            public void onCameraOpened() {
                mUVCCamera.setPreviewSize(640, 480);
                mUVCCamera.startPreview();
                Message msg = new Message();
                msg.what = 6;
                myHandler.sendMessageDelayed(msg,500);
            }

            @Override
            public void onDetached(UsbDevice usbDevice) {
                Log.e("camera","disconnect");
                findViewById(R.id.camera_disconnect).setVisibility(View.VISIBLE);
                mUVCCamera.closeCamera();
            }
        });

        mUVCCamera.setPreviewCallback(new PreviewCallback() {
            @Override
            public void onPreviewFrame(byte[] yuv) {

            }
        });


        mUVCCamera.setPictureTakenCallback(new PictureCallback() {
            @Override
            public void onPictureTaken(String path) {
                Log.e("picpath:+++++++++" , path + " ");
                Message innerMsg = new Message();
                switch (index){
                    case 0:
                        viewModel.img1.set(path);
                        innerMsg.what = 2;
                        myHandler.sendMessageDelayed(innerMsg,100);
                        break;
                    case 1:
                        viewModel.img2.set(path);
                        innerMsg.what = 4;
                        myHandler.sendMessageDelayed(innerMsg,100);
                        break;
                    case 2:
                        viewModel.img3.set(path);
                        viewModel.isClick.set(true);
                        break;
                }
                index++;
            }
        });
    }

    private void setLight(EnumLight light){
        byte [] pdat = new byte[4];
        pdat[0] = 0x0; // 0 for write, 1 for read
        pdat[1] = 0x78; // slave id (same for read and write)
        pdat[2] = (byte) Integer.parseInt(light.getValue(), 16);
        pdat[3] = (byte) Integer.parseInt("ff", 16);
        mUVCCamera.getUVCCamera().nativeXuWrite(0x82, 0xd55b, 4, pdat);
    }
}
