package com.demo.scalp.ui.history;

import android.app.Application;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.databinding.ObservableList;
import android.support.annotation.NonNull;
import android.view.View;

import com.demo.scalp.BR;
import com.demo.scalp.R;
import com.demo.scalp.data.DemoRepository;
import com.demo.scalp.entity.ShowEntity;
import com.demo.scalp.ui.scalpmain.MainActivity;

import me.goldze.mvvmhabit.base.BaseViewModel;
import me.goldze.mvvmhabit.binding.command.BindingAction;
import me.goldze.mvvmhabit.binding.command.BindingCommand;
import me.tatarka.bindingcollectionadapter2.ItemBinding;

public class HistoryViewModel extends BaseViewModel<DemoRepository> {
    public ObservableInt cancelBtn = new ObservableInt(View.INVISIBLE);
    public ObservableInt backV = new ObservableInt(View.VISIBLE);
    public ObservableInt noData = new ObservableInt(View.VISIBLE);
    public ObservableInt moreV = new ObservableInt(View.INVISIBLE);
    public ObservableInt deletePow = new ObservableInt(View.INVISIBLE);
    public ObservableInt deleteS = new ObservableInt(View.INVISIBLE);
    //public ObservableField<ShowEntity> entity = new ObservableField<ShowEntity>();

    public HistoryViewModel(@NonNull Application application, DemoRepository repository) {
        super(application, repository);
    }
    public BindingCommand backIcon = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            finish();
        }
    });
    public BindingCommand moreBtn = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            if (deletePow.get()==View.INVISIBLE){
                deletePow.set(View.VISIBLE);
            }else{
                deletePow.set(View.INVISIBLE);
            }
        }
    });
    public BindingCommand closeSel = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
            deletePow.set(View.INVISIBLE);
        }
    });

}
