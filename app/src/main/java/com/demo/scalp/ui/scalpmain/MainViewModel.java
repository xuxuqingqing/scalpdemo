package com.demo.scalp.ui.scalpmain;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.demo.scalp.R;
import com.demo.scalp.data.DemoRepository;
import com.demo.scalp.entity.DetailEntity;
import com.demo.scalp.entity.ShowEntity;
import com.demo.scalp.entity.UpLoadImageEntity;
import com.demo.scalp.ui.detail.DetailActivity;
import com.google.gson.Gson;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableObserver;
import me.goldze.mvvmhabit.base.BaseViewModel;
import me.goldze.mvvmhabit.binding.command.BindingAction;
import me.goldze.mvvmhabit.binding.command.BindingCommand;
import me.goldze.mvvmhabit.bus.event.SingleLiveEvent;
import me.goldze.mvvmhabit.utils.MaterialDialogUtils;
import me.goldze.mvvmhabit.utils.RxUtils;
import me.goldze.mvvmhabit.utils.ToastUtils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class MainViewModel extends BaseViewModel<DemoRepository> {
    public String image = "";
    public String image_pl = "";
    public String image_uv = "";
    public ObservableField<String> curImg = new ObservableField<String>("");
    public ObservableField<String> httpStr = new ObservableField<String>("");
    public static String baseUrl = "http://18.176.149.96:9090/fileSvr/get/";
    public Resources resources;
    private Context mContext;
    //home visibility
    public ObservableInt homeVisibility = new ObservableInt(View.VISIBLE);
    //scan visibility
    public ObservableInt scanVisiblity = new ObservableInt(View.GONE);

    //封装一个界面发生改变的观察者
    public MainViewModel.UIChangeObservable uc = new MainViewModel.UIChangeObservable();

    public class UIChangeObservable {
        //点击按钮
        public SingleLiveEvent clickDialog = new SingleLiveEvent<>();
    }

    public void RequesetFail(){
        homeVisibility.set(View.GONE);
        scanVisiblity.set(View.VISIBLE);
        image = "";
        image_pl = "";
        image_uv = "";
        index = 0;
        curImg.set("");
        httpStr.set("");
    }

    public MainViewModel(@NonNull Application application, DemoRepository repository) {
        super(application, repository);
        resources = this.getApplication().getBaseContext().getResources();
        mContext=this.getApplication().getBaseContext();
    }
    public void getDetail(String image1,String image2,String image3){
        httpStr.set("正在解析中...");
        model.getDetail(image1,image2,image3)
                .compose(RxUtils.schedulersTransformer()) //线程调度
                .compose(RxUtils.exceptionTransformer()) // 网络错误的异常转换, 这里可以换成自己的ExceptionHandle
                .doOnSubscribe(this)//请求与ViewModel周期同步
                .subscribe(new DisposableObserver<DetailEntity>() {
                    @Override
                    public void onNext(DetailEntity response) {
                        if (response.code == 0) {
                            ShowEntity entity = new ShowEntity(response,resources,baseUrl);
                            Bundle bundle = new Bundle();
                            bundle.putString("entity", new Gson().toJson(entity));
                            bundle.putString("activity","main");
                            startActivity(DetailActivity.class,bundle);
                            finish();
                        } else {
                            //code错误时也可以定义Observable回调到View层去处理
                            ToastUtils.showShort("解析失败");
                            RequesetFail();
                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        RequesetFail();
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }
    public BindingCommand checkPic = new BindingCommand(new BindingAction() {
        @Override
        public void call() {
           uc.clickDialog.call();
        }
    });

    private int index;
    public void uploadImage(final List<String> fileUrl){
        if (index > 2)
        {
            getDetail(image,image_pl,image_uv);
            index = 0;
            return;
        }
        File file = new File(fileUrl.get(index));
        curImg.set(fileUrl.get(index));
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        RequestBody requestApiKey = RequestBody.create(MediaType.parse("multipart/form-data"), "api_app");
        MultipartBody.Part body = MultipartBody.Part.createFormData("name", file.getName(), requestFile);
        model.uploadInmage(body,requestApiKey)
                .compose(RxUtils.schedulersTransformer()) //线程调度
                .compose(RxUtils.exceptionTransformer()) // 网络错误的异常转换, 这里可以换成自己的ExceptionHandle
                .doOnSubscribe(this)//请求与ViewModel周期同步
                .subscribe(new DisposableObserver<UpLoadImageEntity>() {
                    @Override
                    public void onNext(UpLoadImageEntity response) {
                        if (response.code.equals("10000")) {
                            boolean isCallBack = true;
                            if (index == 0) {
                                image = String.format("%s%s",baseUrl,response.img_path);
                                httpStr.set("上传图片(1/3)...");
                            }
                            else if (index == 1) {
                                image_pl = String.format("%s%s",baseUrl,response.img_path);
                                httpStr.set("上传图片(3/3)...");
                            }
                            else if (index == 2) {
                                image_uv = String.format("%s%s",baseUrl,response.img_path);
                                httpStr.set("上传图片(3/3)...");
                            }
                            ++index;
                            uploadImage(fileUrl);
                        } else {
                            //code错误时也可以定义Observable回调到View层去处理
                            ToastUtils.showShort("图片上传失败");
                            RequesetFail();
                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        RequesetFail();
                    }

                    @Override
                    public void onComplete() {
                        dismissDialog();
                    }
                });
    }
}
