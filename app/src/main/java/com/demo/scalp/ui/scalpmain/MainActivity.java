package com.demo.scalp.ui.scalpmain;

import android.Manifest;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.demo.scalp.BR;
import com.demo.scalp.R;
import com.demo.scalp.app.AppViewModelFactory;
import com.demo.scalp.databinding.ScalpMainBinding;
import com.demo.scalp.ui.base.MyBaseActivity;
import com.demo.scalp.ui.camera.CameraActivity;
import com.demo.scalp.ui.detail.DetailActivity;
import com.demo.scalp.ui.history.HistoryActivity;
import com.demo.scalp.utils.AutoUtils;
import com.demo.scalp.utils.Uri2PathUtil;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.List;

import io.reactivex.functions.Consumer;
import me.goldze.mvvmhabit.base.BaseActivity;
import me.goldze.mvvmhabit.utils.ToastUtils;
import me.nereo.multi_image_selector.MultiImageSelector;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;

public class MainActivity extends MyBaseActivity<ScalpMainBinding, MainViewModel> {
    //ActivityLoginBinding类是databinding框架自定生成的,对activity_login.xml
    private static final int REQUEST_IMAGE = 2;

    private static final int REQUEST_TAKEPHOTO = 5;
    @Override
    public int initContentView(Bundle savedInstanceState) {
        return R.layout.scalp_main;
    }

    @Override
    public int initVariableId() {
        return BR.viewModel;
    }

    @Override
    public MainViewModel initViewModel() {
        //使用自定义的ViewModelFactory来创建ViewModel，如果不重写该方法，则默认会调用LoginViewModel(@NonNull Application application)构造方法
        AppViewModelFactory factory = AppViewModelFactory.getInstance(getApplication());
        return ViewModelProviders.of(this, factory).get(MainViewModel.class);
    }

    @Override
    public void initData() {
        super.initData();
    }
    private void RotateAnimation(ImageView view, boolean isFore)
    {
        RotateAnimation rotateAnimation = null;
        if(isFore)
        {
            rotateAnimation = new RotateAnimation(0,360, Animation.RELATIVE_TO_SELF,0.5f,Animation.RELATIVE_TO_SELF,0.5f);
            rotateAnimation.setDuration(6000);
        }
        else
        {
            rotateAnimation = new RotateAnimation(360,0,Animation.RELATIVE_TO_SELF,0.5f,Animation.RELATIVE_TO_SELF,0.5f);
            rotateAnimation.setDuration(5000);
        }
        LinearInterpolator lir = new LinearInterpolator();
        rotateAnimation.setInterpolator(lir);
        rotateAnimation.setRepeatCount(Animation.INFINITE);
        rotateAnimation.setRepeatMode(Animation.RESTART);
        view.startAnimation(rotateAnimation);
    }
    private void moveAnimation(View view) {
        int[] location = new  int[2] ;
        view.getLocationInWindow(location); //获取在当前窗口内的绝对坐标
        view.getLocationOnScreen(location);//获取在整个屏幕内的绝对坐标
        AnimationSet animationSet = new AnimationSet(true);
        TranslateAnimation translateAnimation = new TranslateAnimation(
                android.view.animation.Animation.ABSOLUTE, 0,
                android.view.animation.Animation.ABSOLUTE, 0,
                android.view.animation.Animation.ABSOLUTE,location[1]+AutoUtils.getDisplayHeightValue(460),
                android.view.animation.Animation.ABSOLUTE, location[1]-AutoUtils.getDisplayHeightValue(460));
        translateAnimation.setDuration(500);
        LinearInterpolator lir = new LinearInterpolator();
        translateAnimation.setInterpolator(lir);
        translateAnimation.setRepeatCount(Animation.INFINITE);
        translateAnimation.setRepeatMode(Animation.REVERSE);
        animationSet.addAnimation(translateAnimation);
        view.setAnimation(animationSet);
    }
    @Override
    public void initViewObservable() {
        //监听下拉刷新完成
        viewModel.uc.clickDialog.observe(this, new Observer() {
            @Override
            public void onChanged(@Nullable Object o) {
                requestPermissions();
            }
        });
    }
    public void chosePhotoDialog() {
        final Dialog dialog = new Dialog(this, R.style.ActionSheetDialogStyle);
        View inflate = LayoutInflater.from(this).inflate(R.layout.dialog_photo_select, null);
        dialog.setContentView(inflate);
        Window window = dialog.getWindow();
        if (dialog != null && window != null) {
            window.getDecorView().setPadding(0, 0, 0, 0);
            WindowManager.LayoutParams attr = window.getAttributes();
            if (attr != null) {
                attr.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                attr.width = ViewGroup.LayoutParams.MATCH_PARENT;
                attr.gravity = Gravity.BOTTOM;//设置dialog 在布局中的位置
                window.setAttributes(attr);
            }
        }

        inflate.findViewById(R.id.abroad_history).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, HistoryActivity.class));
                dialog.dismiss();
            }
        });
        inflate.findViewById(R.id.abroad_takephoto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,CameraActivity.class);
                startActivityForResult(intent, REQUEST_TAKEPHOTO);
                dialog.dismiss();
            }
        });

        inflate.findViewById(R.id.abroad_choosephoto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MultiImageSelector.create()
                        .showCamera(false) // 是否显示相机. 默认为显示
                        .count(3) // 最大选择图片数量, 默认为9. 只有在选择模式为多选时有效
                        .single() // 单选模式
                        .multi() // 多选模式, 默认模式;
                        .start(MainActivity.this,REQUEST_IMAGE);

//                Intent intent = new Intent();
//                intent.setAction(Intent.ACTION_PICK);
//                intent.setType("image/*");
//                startActivityForResult(intent, 0);
                dialog.dismiss();
            }
        });
        inflate.findViewById(R.id.abroad_choose_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if(resultCode == RESULT_OK)
            {
                List<String> pathList = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
                if(pathList.size() == 3)
                {
                    getResultOfList(pathList);
                }
                else
                {
                    ToastUtils.showShort("请选择三张图片");
                }
            }
        }

        if (requestCode == REQUEST_TAKEPHOTO) {
            if(resultCode == RESULT_OK)
            {
                List<String> pathList = data.getStringArrayListExtra("ListString");
                getResultOfList(pathList);
            }
        }
    }

    private void getResultOfList(List<String> pathList)
    {
        viewModel.uploadImage(pathList);
        viewModel.homeVisibility.set(View.GONE);
        viewModel.scanVisiblity.set(View.VISIBLE);
        RotateAnimation((ImageView)findViewById(R.id.round1),true);
        RotateAnimation((ImageView)findViewById(R.id.round2),false);
        moveAnimation(findViewById(R.id.scan_line));
        viewModel.httpStr.set("开始上传图片...");
    }

    /**
     * 请求权限
     */
    private void requestPermissions() {
        //请求打开相机权限
        RxPermissions rxPermissions = new RxPermissions(MainActivity.this);
        rxPermissions.request(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean aBoolean) throws Exception {
                        if (aBoolean) {
                            chosePhotoDialog();
                        } else {
                            ToastUtils.showShort("权限被拒绝");
                        }
                    }
                });
    }
}